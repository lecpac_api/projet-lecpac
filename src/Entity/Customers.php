<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContactRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 */
class Customers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $raison_sociale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_contact;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom_contact;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fonction;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $tel_contact;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $portable_contact;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email_contact;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse_2;

    /**
     * @ORM\Column(type="integer")
     */
    private $code_postal_entreprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville_entreprise;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $telephone_entreprise;

    /**
     * @ORM\Column(type="integer")
     */
    private $effectif;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->raison_sociale;
    }

    public function setRaisonSociale(string $raison_sociale): self
    {
        $this->raison_sociale = $raison_sociale;

        return $this;
    }

    public function getNomContact(): ?string
    {
        return $this->nom_contact;
    }

    public function setNomContact(string $nom_contact): self
    {
        $this->nom_contact = $nom_contact;

        return $this;
    }

    public function getPrenomContact(): ?string
    {
        return $this->prenom_contact;
    }

    public function setPrenomContact(string $prenom_contact): self
    {
        $this->prenom_contact = $prenom_contact;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    public function setFonction(string $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getTelContact(): ?string
    {
        return $this->tel_contact;
    }

    public function setTelContact(string $tel_contact): self
    {
        $this->tel_contact = $tel_contact;

        return $this;
    }

    public function getPortableContact(): ?string
    {
        return $this->portable_contact;
    }

    public function setPortableContact(string $portable_contact): self
    {
        $this->portable_contact = $portable_contact;

        return $this;
    }

    public function getEmailContact(): ?string
    {
        return $this->email_contact;
    }

    public function setEmailContact(string $email_contact): self
    {
        $this->email_contact = $email_contact;

        return $this;
    }

    public function getAdresse1(): ?string
    {
        return $this->adresse_1;
    }

    public function setAdresse1(string $adresse_1): self
    {
        $this->adresse_1 = $adresse_1;

        return $this;
    }

    public function getAdresse2(): ?string
    {
        return $this->adresse_2;
    }

    public function setAdresse2(string $adresse_2): self
    {
        $this->adresse_2 = $adresse_2;

        return $this;
    }

    public function getCodePostalEntreprise(): ?int
    {
        return $this->code_postal_entreprise;
    }

    public function setCodePostalEntreprise(int $code_postal_entreprise): self
    {
        $this->code_postal_entreprise = $code_postal_entreprise;

        return $this;
    }

    public function getVilleEntreprise(): ?string
    {
        return $this->ville_entreprise;
    }

    public function setVilleEntreprise(string $ville_entreprise): self
    {
        $this->ville_entreprise = $ville_entreprise;

        return $this;
    }

    public function getTelephoneEntreprise(): ?string
    {
        return $this->telephone_entreprise;
    }

    public function setTelephoneEntreprise(string $telephone_entreprise): self
    {
        $this->telephone_entreprise = $telephone_entreprise;

        return $this;
    }

    public function getEffectif(): ?int
    {
        return $this->effectif;
    }

    public function setEffectif(int $effectif): self
    {
        $this->effectif = $effectif;

        return $this;
    }
}
