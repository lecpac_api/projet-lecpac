<?php


namespace App\DataFixtures;


use App\Entity\Customers;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CustomerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //Create contacts
        $contact1 = new Customers();
        $contact1->setRaisonSociale("SNCF");
        $contact1->setNomContact("DURAND");
        $contact1->setPrenomContact("Pierre");
        $contact1->setCivilite("M");
        $contact1->setFonction("Président");
        $contact1->setTelContact("04 45 78 78 78");
        $contact1->setPortableContact("06 78 45 15 45");
        $contact1->setEmailContact("durand@sncf.fr");
        $contact1->setAdresse1("180 RUE LOUIS ARMAND");
        $contact1->setAdresse2("Bat. Hemiris A");
        $contact1->setCodePostalEntreprise(13290);
        $contact1->setVilleEntreprise("AIX EN PROVENCE");
        $contact1->setTelephoneEntreprise("04 78 45 45 45");
        $contact1->setEffectif(25);

        $contact2 = new Customers();
        $contact2->setRaisonSociale("SNCF");
        $contact2->setNomContact("DUPUIS");
        $contact2->setPrenomContact("Laurent");
        $contact2->setCivilite("M");
        $contact2->setFonction("D.G.");
        $contact2->setTelContact("04 48 75 41 69");
        $contact2->setPortableContact("06 78 45 45 12");
        $contact2->setEmailContact("dupuis@sncf.fr");
        $contact2->setAdresse1("180 RUE LOUIS ARMAND");
        $contact2->setCodePostalEntreprise(13290);
        $contact2->setVilleEntreprise("AIX EN PROVENCE");
        $contact2->setTelephoneEntreprise("04 78 45 45 45");
        $contact2->setEffectif(25);

        $contact3 = new Customers();
        $contact3->setRaisonSociale("EDF");
        $contact3->setNomContact("LEROY");
        $contact3->setPrenomContact("Estelle");
        $contact3->setCivilite("Mme");
        $contact3->setFonction("Gérante");
        $contact3->setTelContact("04 75 78 78 74");
        $contact3->setPortableContact("06 15 78 78 45");
        $contact3->setEmailContact("leroy@edf.fr");
        $contact3->setAdresse1("180 RUE DES LILAS");
        $contact3->setCodePostalEntreprise(75001);
        $contact3->setVilleEntreprise("PARIS");
        $contact3->setTelephoneEntreprise("04 78 45 45 45");
        $contact3->setEffectif(50);

        //Persist into Database
        $manager->persist($contact1);
        $manager->persist($contact2);
        $manager->persist($contact3);

        $manager->flush();


    }
}
