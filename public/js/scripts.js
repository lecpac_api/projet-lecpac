const url = 'http://localhost:8000/api/customers';

getAllClient();

//Création d'un nouveau client
$("#create").click(function ()
{
    const client = {
        raisonSociale: "AFTI",
        nomContact: "Martinet",
        prenomContact: "Rémi",
        civilite: "M",
        fonction: "Etudiant",
        telContact: "06 68 74 12 59",
        portableContact: "06 68 74 12 59",
        emailContact: "remi10martinet11@gmail.com",
        adresse1: "180 RUE LOUIS ARMAND",
        adresse2: "Bat. Hemiris A",
        codePostalEntreprise: 92130,
        villeEntreprise: "ISSY-LES-MOULINEAUX",
        telephoneEntreprise: "04 78 45 45 45",
        effectif: 3
    };

    const options = {
        method: 'POST',
        body: JSON.stringify(client),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    fetch(url , options)
        .then(res => res.json())
        .then((res) =>
        {
            $("#message").html("<div class='alert alert-success' role='alert'> Client ajouté: " + res.nomContact + " " + res.prenomContact + "</div>");
            getAllClient();
        });
})

//Affiche la listes des clients
function getAllClient()
{
    $("#tabClients").empty();

    //Recupére les les données avec l'API
    fetch(url)
        .then(response => response.json())
        .then((data) =>
        {
            console.log(data);

            $.each(data, function (key, unClient)
            {
                var monClient = "<tr><th scope='row'>" + unClient.nom_contact + "</th> " +
                                "<td>" + unClient.prenom_contact + "</td>" +
                                "<td>" + unClient.raisonSociale + "</td>" +
                                "<td class='thCenter'> <button type='button' class='btnDetail btn btn-primary' data-id='" + unClient.id + "'>Détails</button></td>" +
                                "<td class='thCenter'> <button type='button' class='btnUpdate btn btn-warning' data-id='" + unClient.id + "'>Modifier</button></td>" +
                                "<td class='thCenter'> <button type='button' class='btnDelete btn btn-danger' data-id='" + unClient.id + "'>Supprimer</button></td>" +
                                "</tr>"

                $("#tabClients").append(monClient);
            })

            //Affiche les détails d'un client grâce à son ID
            $(".btnDetail").click(function ()
            {
                getDetailClient($(this).attr("data-id"))
            })

            function getDetailClient(idClient)
            {
                fetch(url + '/' + idClient)
                    .then(response => response.json())
                    .then((data) =>
                    {
                        console.log(data);
                        $("#infosClient").removeAttr('hidden');

                        $("#civilite").html(createInput("Civilité", data.civilite));
                        $("#nom").html(createInput("Nom", data.nomContact));
                        $("#prenom").html(createInput("Prénom", data.prenomContact))
                        $("#entreprise").html(createInput("Entreprise", data.raisonSociale));
                        $("#fonction").html(createInput("Fonction", data.fonction));
                        $("#tel").html(createInput("Téléphone contact", data.telContact));
                        $("#portable").html(createInput("Portable contact", data.portableContact));
                        $("#email").html(createInput("Email", data.emailContact));

                        if(!data.adresse2)
                        {
                            data.adresse2 = "";
                        }

                        $("#adresse1").html(createInput("Adresse 1", data.adresse1));
                        $("#adresse2").html(createInput("Adresse 2", data.adresse2))
                        $("#cp").html(createInput("Code postal", data.codePostalEntreprise))
                        $("#ville").html(createInput("Ville", data.villeEntreprise));
                        $("#telEntreprise").html(createInput("Téléphone entreprise", data.telephoneEntreprise));
                        $("#effectif").html(createInput("Effectif", data.effectif));

                        function createInput(name, infos)
                        {
                            var monInput = "<div class='input-group input-group-sm mb-3'>" +
                                "  <div class='input-group-prepend'>" +
                                "    <span class='input-group-text' id='inputGroup-sizing-sm'>" + name + "</span>" +
                                "  </div>" +
                                "  <input type='text' value='" + infos + "' class='form-control' aria-label='Small' aria-describedby='inputGroup-sizing-sm' readOnly>" +
                                "</div>"

                            return monInput;
                        }


                    })
            }

            //Modifie l'adresse1 du client par son id
            $(".btnUpdate").click(function ()
            {
                const client = {
                    adresse1: 'test'
                };

                const options = {
                    method: 'PUT',
                    body: JSON.stringify(client),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }

                fetch(url + "/" + $(this).attr('data-id'), options)
                    .then(res => res.json())
                    .then((res) =>
                    {
                        $("#message").html("<div class='alert alert-success' role='alert'> Client Modifié: Adresse1: " + res.adresse1 + "</div>")
                        console.log(res)
                        console.log($(this).attr('data-id'))
                        getDetailClient($(this).attr('data-id'))
                    });
            })

            //Supprime un client avec son ID
            $(".btnDelete").click(function ()
            {
                fetch(url + "/" + $(this).attr("data-id"), {method: 'DELETE'})
                    .then(res =>
                    {
                        if (res.ok)
                        {
                            $(this).closest("tr").remove();

                            $("#message").html("<div class='alert alert-success' role='alert'> Client supprimé !</div>")
                        }
                        else
                            {
                                $("#message").html("<div class='alert alert-danger' role='alert'> Erreur durant la suppression </div>")
                            }
                    })
                    .then(res => console.log(res));
            })
        })


}
