# LECPAC-API

Projet effectué pour le poste d'apprenti en développement web chez LECPAC Consulting.

## Lancer le projet

Docker est requis pour lancer le projet, [voir comment installer Docker (et docker-compose)](https://docs.docker.com/get-docker/).
  
Création de l'image docker:
```shell
docker-compose up
```

Mise à jour de la base de données:
```shell
docker-compose exec php-fpm bin/console doctrine:migration:migrate --no-interaction
```

Insertion des fixtures dans la base de données:
```shell
docker-compose exec php-fpm bin/console doctrine:fixtures:load
```

Aprés avoir éxécuté ces commandes, vous pouvez accéder au site sur cette adresse  [localhost:8000](localhost:8000)
Pour avoir accés à la [base de donnée](localhost:8080)(User: root / Password: root)

